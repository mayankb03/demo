//
//  Emp.swift
//  RealmDemo
//
//  Created by Jenkins_New on 7/15/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import RealmSwift

class Emp: Object {
    
    dynamic var empNo = 0
    dynamic var deptno = 0
    dynamic var deptName = ""
    dynamic var username = ""
    dynamic var password = ""
    dynamic var firstName = ""
    dynamic var lastName = ""
    
    override static func primaryKey() -> String? {
        return "empNo"
    }
    
}