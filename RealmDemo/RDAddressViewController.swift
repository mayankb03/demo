//
//  AddressViewController.swift
//  RealmDemo
//
//  Created by Jenkins_New on 7/20/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit
import RealmSwift

class AddressViewController: UIViewController {
    
    var empNo: Int!
    
    @IBOutlet weak var addressLine1: UITextField!
    @IBOutlet weak var addressLine2: UITextField!
    @IBOutlet weak var addressLine3: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveAddress(sender: AnyObject) {
        let address = RDEmployeeAddress()
        if checkValidation() == true {
            
            address.addressLine1 = addressLine1.text!
            address.addressLine2 = addressLine2.text!
            address.zipcode = Int(addressLine3.text!)!
            address.empNo = self.empNo
            
            let realm = try! Realm()
            
            try! realm.write() {
                realm.add(address)
            }
        }
    }
    
    func checkValidation() -> Bool {
        var ok: Bool! = false
        if addressLine1.text?.isEmpty == false && addressLine2.text?.isEmpty == false && addressLine3.text?.isEmpty == false {
            ok = true
        }
        return ok
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
