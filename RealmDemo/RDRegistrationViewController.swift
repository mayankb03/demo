//
//  RegisterViewController.swift
//  RealmDemo
//
//  Created by Jenkins_New on 7/15/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit
import RealmSwift

class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: Properties
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var deptName: UIPickerView!
    @IBOutlet weak var reg_upd_button: UIButton!
    
    var rowSelect: Int! = 0
    var empNo: Int! = 0
    var isInUpdateMode: Bool! = false
    var e = Emp()
    
    var dept = ["Marketting", "Purchasing", "Shipping", "Customer Relations", "Inventory"]
    var deptNo = [10, 20, 30, 40, 50]
    
    //MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if self.isInUpdateMode == true {
            self.title = "Update"
            reg_upd_button.setTitle("Update", forState: .Normal)
            fillTextFields()
        }
    }
    
    func fillTextFields() {
        firstName.text = e.firstName
        lastname.text = e.lastName
        userName.text = e.username
        password.text = e.password
    }
    
    //MARK: UIPickerView methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dept.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dept[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rowSelect = row
    }
    
    //MARK: when clicking the register button.
    @IBAction func doRegister(sender: AnyObject) {
        if isInUpdateMode == true {
            if checkValidation() == true {
                let _empNo = e.empNo
                updateEmp(_empNo)
            }else {
                showAlert("All fields are mandatory")
            }
        } else {
            if checkValidation() == true {
                if checkExistence() == true {
                    saveEmp()
                    //                showAlert("Successful.")
                }else {
                    showAlert("User with same password exists.")
                }
            }else {
                showAlert("All fields are mandatory")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //checking validations on all the textfields.
    func checkValidation() -> Bool {
        var result: Bool! = false
        if (firstName.text?.isEmpty)!==false && (lastname.text?.isEmpty)!==false && (userName.text?.isEmpty)!==false
            && (password.text?.isEmpty)!==false {
            result = true
        }
        return result
    }
    
    //Alert message.
    func showAlert(message: String!) {
        
        let alert = UIAlertController(title: "Alert",
                                      message: message,
                                      preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "OK",
                                       style: .Default,
                                       handler: { (action:UIAlertAction) -> Void in
                                        
        })
        
//        let cancelAction = UIAlertAction(title: "Cancel",
//                                         style: .Default) { (action: UIAlertAction) -> Void in
//        }
        
        alert.addAction(saveAction)
//        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    //Inserting into Realm object.
    func saveEmp() {
        let emp = Emp()
        emp.firstName = firstName.text!
        emp.lastName = lastname.text!
        emp.password = password.text!
        emp.username = userName.text!
        emp.deptName = self.dept[self.rowSelect]
        emp.deptno = self.deptNo[self.rowSelect]
        
        let realm = try! Realm()
        
        try! realm.write() {
            //    let empNo = realm.objects(Emp.self).last?.empNo
            let empNo = realm.objects(Emp.self).sorted("empNo", ascending: false)
            if empNo.count != 0 {
                emp.empNo = empNo[0].empNo + 1
            }
        }
        self.empNo = emp.empNo
        
        try! realm.write() {
            realm.add(emp)
        }
    }
    
    func updateEmp(e: Int) {
        
        let _update = Emp()
        _update.firstName = firstName.text!
        _update.lastName = lastname.text!
        _update.username = userName.text!
        _update.password = password.text!
        _update.deptName = dept[self.rowSelect]
        _update.deptno = deptNo[self.rowSelect]
        _update.empNo = self.e.empNo
        
        self.empNo = self.e.empNo
        
        let realm = try! Realm()
        try! realm.write() {
        realm.add(_update, update: true)
        }
    }
    
    //Reading all the realm objects and filtering results.
    func checkExistence() -> Bool {
        var ok: Bool! = true
        
        let realm = try! Realm()
        let result = realm.objects(Emp.self).filter("username == '\(userName.text!)'")
        for i in result {
            print("password: \(i.password)")
            if password.text?.compare(i.password) == .OrderedSame {
                ok = false
                break
            }
        }
        return ok
    }
    
    /*
     MARK: - Navigation
 */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "registersegue" {
            let destination = segue.destinationViewController as! ListViewController
            destination.empNo = self.empNo
            self.navigationController?.popToRootViewControllerAnimated(false)
        }
    }
}
