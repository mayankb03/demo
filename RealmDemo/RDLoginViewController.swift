//
//  ViewController.swift
//  RealmDemo
//
//  Created by Jenkins_New on 7/15/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    var empNo: Int! = 0
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
//        showAlert()
    }
    
    @IBAction func doLogin(sender: AnyObject) {
        if checkValidation() == true {
            let realm = try! Realm()
            var loginOk: Bool! = false
            for i in realm.objects(Emp.self).filter("username == '\(userName.text!)'") {
                if passWord.text?.compare(i.password) == .OrderedSame {
                    self.empNo = i.empNo
                    //                showAlert("Successful.")
                    loginOk = true
                    break
                }
            }
            if loginOk == false {
                showAlert("Either incorrect username or password.")
            }
        }else {
            showAlert("All fields are mandatory.")
        }
    }
    
    //checking validations on all the textfields.
    func checkValidation() -> Bool {
        var result: Bool! = false
        if (userName.text?.isEmpty)! == false && (passWord.text?.isEmpty)! == false {
            result = true
        }
        return result
    }
    
    //Alert message.
    func showAlert(message: String!) {
        
        let alert = UIAlertController(title: "Alert",
                                      message: message,
                                      preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "OK",
                                       style: .Default,
                                       handler: { (action:UIAlertAction) -> Void in
                                        
        })
        
        //        let cancelAction = UIAlertAction(title: "Cancel",
        //                                         style: .Default) { (action: UIAlertAction) -> Void in
        //        }
        
        alert.addAction(saveAction)
        //        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "loginsegue" {
            let destination = segue.destinationViewController as! ListViewController
            destination.empNo = self.empNo
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
}

