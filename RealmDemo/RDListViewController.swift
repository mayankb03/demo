//
//  ListViewController.swift
//  RealmDemo
//
//  Created by Jenkins_New on 7/18/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit
import RealmSwift

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var empNo: Int!
    
    @IBOutlet weak var tab: UITableView!
    
    let realm = try! Realm()
    var results = try! Realm().objects(Emp.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("inside listViewController with emp: \(empNo)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView DataSource methods.
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        cell.textLabel?.text = results[indexPath.row].firstName + " " + results[indexPath.row].lastName
        return cell
    }

    //TableView Delegate methods.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if results[indexPath.row].empNo == self.empNo {
//            showAlert("Editable.")
            self.performSegueWithIdentifier("updatesegue", sender: self)
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        if self.empNo == results[indexPath.row].empNo {
            let delete = UITableViewRowAction(style: .Default, title: "Delete") { action, index in
            print("delete")
            try! self.realm.write() {
                self.realm.delete(self.results[indexPath.row])
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tab.reloadData()
                self.tab.setContentOffset(CGPointZero, animated: true)
            }
        }
//            let edit = UITableViewRowAction(style: .Default, title: "Edit") { action, index in
//                print("Edit")
//                self.showAlert("Will show an editbox soon.")
//                TODO: implement the editing thing here.
//                self.performSegueWithIdentifier("updatesegue", sender: self)
//            }
            return [delete]
        }else {
            let other = UITableViewRowAction(style: .Normal, title: "Cancel") { action, index in
            }
            return [other]
        }
    }
    
    //Alert message.
    func showAlert(message: String!) {
        
        let alert = UIAlertController(title: "Alert",
                                      message: message,
                                      preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "OK",
                                       style: .Default,
                                       handler: { (action:UIAlertAction) -> Void in
                                        
        })
        
        //        let cancelAction = UIAlertAction(title: "Cancel",
        //                                         style: .Default) { (action: UIAlertAction) -> Void in
        //        }
        
        alert.addAction(saveAction)
        //        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation
*/
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "updatesegue" {
            let destination = segue.destinationViewController as! RegisterViewController
            destination.isInUpdateMode = true
            let curIndex = self.tab.indexPathForSelectedRow!
            destination.e = results[curIndex.row]
        }
    }

}
